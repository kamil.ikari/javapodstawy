import java.util.Scanner;

public class Matryca {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj wielkosc macierzy :");
        int x = Integer.valueOf(scan.nextLine());

        System.out.println("");

        int[][] tablica = new int[x][x];

        for (int i = 0; i < tablica.length; i++) {
            for (int j = 0; j < tablica[i].length; j++) {
                if (i == j){
                    tablica[i][j]=1;
                }
                System.out.print(tablica[i][j] + " ");
            }
            System.out.println("");
        }
    }
}
